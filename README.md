# Chat Node.Js Server

This is [Node.js](https://nodejs.org/en), [TypeScript](https://www.typescriptlang.org) application of chat where you can create rooms and join to different rooms and chat with people there. 

## Setup

### Local Project Setup

Install packages

```
npm install
```

#### How to run

Run local for development

```
npm run start:dev
```

Run as production

```
npm run build
```
copy ```configs/config.yaml``` file into ``` dist/configs ``` folder
```
npm run start:prod
```


#### How to connect socket
Credentials are in ``` configs/config.yaml ``` file.

Events are described in ``` app/api/websocket.json ``` swagger file.
