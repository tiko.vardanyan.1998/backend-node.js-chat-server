import { Schema } from 'convict';

import { Config } from './interfaces';

export const configSchema: Schema<Config> = {
  app: {
    jwt: {
      secret: {
        default: '',
        format: String,
        nullable: false,
        env: 'JWT_SECRET',
      },
      algorithm: {
        default: 'HS512',
        format: String,
        env: 'JWT_ALGORITHM',
      },
      expire: {
        access: {
          default: '10m',
          format: String,
          env: 'JWT_EXPIRE_ACCESS',
        },
        refresh: {
          default: '10d',
          format: String,
          env: 'JWT_EXPIRE_REFRESH',
        },
      },
    },
  },
  transports: {
    ws: {
      port: {
        default: 3457,
        env: 'TRANSPORTS_WS_PORT',
        format: 'port',
      },
      host: {
        default: '127.0.0.1',
        env: 'TRANSPORTS_WS_HOST',
      },
      compression: {
        default: 0,
        env: 'TRANSPORTS_WS_COMPRESSION',
        format: 'int',
      },
      maxPayloadLength: {
        default: 1,
        env: 'TRANSPORTS_WS_MAX_PAYLOAD',
        format: 'int',
      },
      idleTimeout: {
        default: 1,
        env: 'TRANSPORTS_WS_TIMEOUT',
        format: 'int',
      },
      authTimeout: {
        default: 1,
        env: 'TRANSPORTS_WS_AUTH_TIMEOUT',
        format: 'int',
      },
    },
    http: {
      port: {
        default: 3000,
        env: 'TRANSPORTS_HTTP_PORT',
        format: 'port',
      },
      host: {
        default: '127.0.0.1',
        env: 'TRANSPORTS_WS_HOST',
      }
    },
  },
  resources: {
    redis: {
      host: {
        default: '127.0.0.1',
        format: String,
        env: 'REDIS_HOST',
      },
      port: {
        default: 6379,
        format: 'port',
        env: 'REDIS_PORT',
      },
      db: {
        default: 0,
        format: Number,
        env: 'REDIS_DB',
      },
      password: {
        format: String,
        env: 'REDIS_PASSWORD',
        default: '',
      },
      sentinel: {
        enabled: {
          format: Boolean,
          env: 'REDIS_SENTINEL_ENABLED',
          default: true,
        },
        name: {
          format: String,
          env: 'REDIS_SENTINEL_NAME',
          default: 'freightmax',
        },
        password: {
          format: String,
          env: 'REDIS_SENTINEL_PASSWORD',
          default: '',
        },
        port: {
          format: 'port',
          env: 'REDIS_SENTINEL_PORT',
          default: 6379,
        },
        hosts: {
          format: String,
          env: 'REDIS_SENTINEL_HOSTS',
          default: '',
        },
      },
    },
    mongo: {
      nodes: {
        default: 'mongodb://localhost:27017',
        env: 'MONGO_HOSTS',
        format: String,
      },
      db: {
        default: 'users',
        env: 'MONGO_DB',
        format: String,
      },
      auth: {
        enabled: {
          default: false,
          env: 'MONGO_AUTH_ENABLED',
          format: Boolean,
        },
        source: {
          default: 'admin',
          env: 'MONGO_AUTH_SOURCE',
          format: String,
        },
        user: {
          default: 'root',
          env: 'MONGO_AUTH_USER',
          format: String,
        },
        password: {
          default: '',
          env: 'MONGO_AUTH_PASSWORD',
          format: String,
        },
      },
    },
  },
  logger: {
    levels: {}
  },
};
