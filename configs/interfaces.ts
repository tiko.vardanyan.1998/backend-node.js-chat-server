export interface TransportWebSocketConfig {
  host: string;
  port: number;
  compression: number;
  maxPayloadLength: number;
  idleTimeout: number;
  authTimeout: number;
}

export interface TransportHttpConfig {
  host: string;
  port: number;
}

export interface TransportsConfig {
  ws: TransportWebSocketConfig;
  http: TransportHttpConfig;
}

export interface ResourceRedisSentinelConfig {
  enabled: boolean;
  name: string;
  hosts: string;
  port: number;
  password: string;
}

export interface ResourceRedisConfig {
  host: string;
  port: number;
  db: number;
  password: string;
  sentinel: ResourceRedisSentinelConfig;
}

export interface ResourceMongoAuthConfig {
  user: string;
  password: string;
  enabled: boolean;
  source: string;
}

export interface ResourceMongoConfig {
  nodes: string;
  db: string;
  auth: ResourceMongoAuthConfig;
}

export interface ResourcesConfig {
  redis: ResourceRedisConfig;
  mongo: ResourceMongoConfig;
}

export interface AppJwtExpireConfig {
  access: string;
  refresh: string;
}

export interface AppJwtConfig {
  secret: string;
  expire: AppJwtExpireConfig;
  algorithm: string;
}

export interface AppConfig {
  jwt: AppJwtConfig;
}

export interface LoggerConfig {
  levels: Record<string, string>;
}

export interface Config {
  app: AppConfig;
  transports: TransportsConfig;
  resources: ResourcesConfig;
  logger: LoggerConfig;
}
