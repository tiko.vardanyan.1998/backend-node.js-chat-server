import { Schema } from 'mongoose';

import { SendMessage, Message as MessageDomain } from '../../../domains/message';

import { RepositoryTypes } from '../../../libraries/constants/repository-types';

import { MongoBaseRepository } from '../../mongo-base';

import {
  MessageMongoDocument,
  MessageMongo,
} from './interfaces';
import { RequestOptions } from '../../../domains/request-options';

export class MongoMessageRepository
  extends MongoBaseRepository<Schema<MessageMongoDocument>, MessageDomain>
  implements MessageMongo {

  public get type(): RepositoryTypes {
    return RepositoryTypes.message;
  }

  public async create(sendMessage: SendMessage, userId: string): Promise<MessageMongoDocument> {
    const result = await this.collection.create({
      user_id: userId,
      room: sendMessage.room,
      message: sendMessage.message,
      created_at: Date.now(),
    });

    return result as unknown as MessageMongoDocument;
  }

  public async getMessages(room: string, options: RequestOptions): Promise<Array<MessageDomain>> {
    const result = await this.collection
      .find({ room })
      .sort({ 'created_at': 'desc' })
      .skip(options.skip)
      .limit(options.limit || 10)
      .lean<Array<MessageMongoDocument>>();

    return result.map((item) => this.deserialize(item));
  }

  public async deleteMessages(ids: Array<string>, userId: string): Promise<number> {
    const result = await this.collection
      .deleteMany({
        _id: { $in: ids },
        user_id: userId,
      });

    return result.deletedCount;
  }

  protected get collectionName(): string {
    return 'messages';
  }

  protected get schema(): Schema {
    return new Schema<MessageMongoDocument>({
      user_id: {
        type: String,
        required: true,
      },
      room: {
        type: String,
        required: true,
      },
      message: {
        type: String,
        required: true,
      },
      created_at: {
        type: String,
      },
    });
  }

  protected deserialize(raw: MessageMongoDocument): MessageDomain {
    return {
      id: raw._id.toString(),
      userId: raw.user_id,
      room: raw.room,
      message: raw.message,
      createdAt: new Date(+raw.created_at),
    };
  }
}
