import { Document } from 'bson';

import {
  SendMessage,
  Message as MessageDomain,
} from '../../../domains/message';

import { Storage } from '../../interfaces/storage';
import { RequestOptions } from '../../../domains/request-options';


export interface MessageMongoDocument extends Document {
  id: string;
  user_id: string;
  room: string;
  message: string;
  created_at: string;
}

export interface MessageMongo extends Storage<MessageDomain> {
  create(Message: SendMessage, userId: string): Promise<MessageMongoDocument>;
  getMessages(room: string, options: RequestOptions): Promise<Array<MessageDomain>>;
  deleteMessages(ids: Array<string>, userId: string): Promise<number>;
}

export type Message = MessageMongo;
