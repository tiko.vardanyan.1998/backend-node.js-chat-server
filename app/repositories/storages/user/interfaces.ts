import { Document } from 'bson';

import {
  CreateUser,
  User as UserDomain,
} from '../../../domains/user';

import { Storage } from '../../interfaces/storage';


export interface UserMongoDocument extends Document {
  id: string;
  first_name: string;
  last_name: string;
  username: string;
  password: string;
  created_at: Date;
  updated_at: Date;
}

export interface UserMongo extends Storage<UserDomain> {
  create(user: CreateUser): Promise<{ userId: string }>;
  findByUsername(username: string): Promise<UserMongoDocument>;
}

export type User = UserMongo;
