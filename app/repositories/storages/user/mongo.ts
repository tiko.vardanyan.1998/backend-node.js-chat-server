import { Schema } from 'mongoose';

import { CreateUser, User as UserDomain } from '../../../domains/user';

import { RepositoryTypes } from '../../../libraries/constants/repository-types';

import { MongoBaseRepository } from '../../mongo-base';

import { UserMongo, UserMongoDocument } from './interfaces';
import { ErrorMessages } from '../../../libraries/constants/error-messages';
import { ClientError } from '../../../libraries/errors/client';
import { HttpStatusCodes } from '../../../libraries/constants/http-status-codes';

export class MongoUserRepository
  extends MongoBaseRepository<Schema<UserMongoDocument>, UserDomain>
  implements UserMongo {
  public get type(): RepositoryTypes {
    return RepositoryTypes.user;
  }

  public async create(createUser: CreateUser): Promise<{ userId: string }> {
    const usernameExists = await this.collection.findOne({ username: createUser.username });

    if (usernameExists) {
      throw new ClientError(ErrorMessages.alreadyExistsUsername, HttpStatusCodes.badRequest);
    }

    const result = await this.collection.create({
      first_name: createUser.firstName,
      last_name: createUser.lastName,
      username: createUser.username,
      password: createUser.password,
    });

    return { userId: result._id.toString() };
  }

  public async findByUsername(username:string): Promise<UserMongoDocument> {
    const user = await this.collection.findOne({
      username,
    }).lean<UserMongoDocument>()

    if (!user) {
      throw new ClientError(ErrorMessages.userNotFound, HttpStatusCodes.badRequest);
    }

    this.log.debug(user);

    return user;
  }

  protected get collectionName(): string {
    return 'users';
  }

  protected get schema(): Schema {
    return new Schema<UserMongoDocument>({
      first_name: {
        type: String,
        required: true,
      },
      last_name: {
        type: String,
        required: true,
      },
      username: {
        type: String,
        required: true,
        unique: true,
      },
      password: {
        type: String,
        required: true,
      },
      created_at: {
        type: Date,
        default: Date.now(),
      },
      updated_at: {
        type: Date,
        default: Date.now(),
        required: true,
      },
    });
  }

  protected deserialize(raw: UserMongoDocument): UserDomain {
    return {
      id: raw._id.toString(),
      firstName: raw.first_name,
      lastName: raw.last_name,
      username: raw.username,
      createdAt: raw.created_at,
      updatedAt: raw.updated_at,
    };
  }
}
