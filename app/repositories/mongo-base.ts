import {
  Model, model, Schema,
} from 'mongoose';
import { Document } from 'bson';

import { MongoResource } from '../resources/mongo';

import { Base } from '../domains/base';

import { BaseRepository } from './base';
import { Storage } from './interfaces/storage';


export class MongoBaseRepository<S extends Schema, E extends Base>
  extends BaseRepository<MongoResource>
  implements Storage<E> {
  protected model?: Model<S>;

  protected get collectionName(): string {
    throw new Error(
      'Not implemented mongo repository collection name',
    );
  }

  public get storageName(): string {
    return this.collectionName;
  }

  protected get schema(): Schema {
    throw new Error(
      'Not implemented mongo repository collection name',
    );
  }

  protected deserialize(raw: Document): E {
    throw new Error(
      'Not implemented mongo repository deserialize method',
    );
  }

  protected get collection(): Model<S> {
    if (!this.model) {
      this.model = model<S>(
        this.collectionName,
        this.schema,
      );
    }

    return this.model;
  }
}
