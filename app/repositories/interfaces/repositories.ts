import { User } from '../storages/user/interfaces';
import { Message } from '../storages/messages/interfaces';

export interface Repositories {
  readonly users: User;
  readonly messages: Message;
}
