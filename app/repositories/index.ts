import { Logger as PinoLogger } from 'pino';

import { Logger } from '../libraries/logger';

import { RepositoryTypes } from '../libraries/constants/repository-types';

import { Resources } from '../resources/interfaces';
import { BaseResource } from '../resources/base';

import { BaseRepository } from './base';

import { Repositories as RepositoriesInterface } from './interfaces/repositories';
import { MongoUserRepository } from './storages/user/mongo';
import { User } from './storages/user/interfaces';
import { MongoMessageRepository } from './storages/messages/mongo';
import { Message } from './storages/messages/interfaces';


export class Repositories implements RepositoriesInterface {
  private readonly log: PinoLogger;

  private readonly logger: Logger;

  private readonly repositories:
    Partial<Record<RepositoryTypes, BaseRepository<BaseResource>>>;

  constructor(resources: Resources, logger: Logger) {
    this.logger = logger;
    this.log = logger.getLogger('repositories');

    this.repositories = {};
    this.init(resources)
      .forEach((repository) => {
        if (!this.repositories[repository.type]) {
          this.repositories[repository.type] = repository;
        }
      });
  }

  public get users(): User {
    const repository = this.repositories[RepositoryTypes.user];

    if (!repository) {
      throw new Error('Repository not initialized');
    }

    return repository as unknown as User;
  }

  public get messages(): Message {
    const repository = this.repositories[RepositoryTypes.message];

    if (!repository) {
      throw new Error('Repository not initialized');
    }

    return repository as unknown as Message;
  }

  private init(resources: Resources): Array<BaseRepository<BaseResource>> {
    return [
      new MongoUserRepository(this, resources.mongo, this.logger),
      new MongoMessageRepository(this, resources.mongo, this.logger),
    ];
  }
}
