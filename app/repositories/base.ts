import { Logger as PinoLogger } from 'pino';
import _ from 'lodash';

import { Logger } from '../libraries/logger';

import { RepositoryTypes } from '../libraries/constants/repository-types';


import { BaseResource } from '../resources/base';


import { Repositories } from './interfaces/repositories';
import { Repository } from './interfaces/repository';

export class BaseRepository<T extends BaseResource> implements Repository {
  private static readonly sequences: Partial<Record<RepositoryTypes, boolean>> = {};

  protected readonly db: T;

  protected readonly log: PinoLogger;

  protected readonly repositories: Repositories;

  constructor(repositories: Repositories, db: T, logger: Logger) {
    this.db = db;
    this.repositories = repositories;
    this.log = logger.getLogger(
      _.kebabCase(this.constructor.name),
    );
  }

  public get type(): RepositoryTypes {
    throw new Error('Repository type getter not implemented');
  }
}
