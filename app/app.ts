import convict from 'convict';
import { load as parseYaml } from 'js-yaml';
import { Logger as PinoLogger } from 'pino';
import EventEmitter from 'events';

import path from 'path';

import { Config } from '../configs/interfaces';
import { Logger } from './libraries/logger';

import { Resources as ResourcesInterface } from './resources/interfaces';
import { configSchema } from '../configs/constants';
import { Transports } from './transports';
import { Resources } from './resources';
import { Services } from './services';
import { Repositories } from './repositories';
import { Jwt } from './libraries/jwt';

export class AppServer {
  private readonly rootPath: string;

  private config?: Config;

  private resources?: ResourcesInterface;

  private transports?: Transports;

  private services?: Services;

  private readonly logger: Logger;

  private readonly log: PinoLogger;

  constructor(rootPath: string) {
    this.rootPath = path.resolve(rootPath);

    this.loadConfig();

    this.logger = new Logger(this.getConfig().logger);
    this.log = this.logger.getLogger();
  }

  public getConfig(): Config {
    if (!this.config) {
      throw new Error('Application config not loaded');
    }

    return this.config;
  }

  public async stop(): Promise<void> {
    await this.resources?.stop();
    await this.transports?.stop();
    await this.services?.stop();
  }

  public async start(): Promise<void> {
    await this.resources?.start();
    await this.transports?.start();
    await this.services?.start();
  }

  public async init(): Promise<void> {
    const config = this.getConfig();
    const eventBus = new EventEmitter();

    this.resources = new Resources(config.resources, this.logger);

    const repositories = new Repositories(this.resources, this.logger);

    const jwt = new Jwt(config.app.jwt);

    const services = new Services(
      repositories,
      this.resources,
      this.logger,
      config.app,
      jwt,
      eventBus,
    );

    this.transports = new Transports(
      services,
      config.transports,
      this.logger,
      jwt,
      eventBus,
    );
  }

  private loadConfig(): void {
    convict.addParser({
      extension: [
        'yaml',
      ],
      parse: parseYaml,
    });
    const config = convict(configSchema);

    config.loadFile(path.join(this.rootPath, 'configs', 'config.yaml'));

    config.validate({
      allowed: 'strict',
    });

    this.config = config.getProperties();
  }


}
