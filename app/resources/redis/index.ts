import IORedis, { Redis as RedisBase, RedisOptions } from 'ioredis';

import { ResourceRedisConfig } from '../../../configs/interfaces';

import { Logger } from '../../libraries/logger';
import { ResourceType } from '../../libraries/constants/resource-types';

import { BaseResource } from '../base';
import { Resource } from '../resource';

export class RedisResource extends BaseResource implements Resource {
  private readonly client: RedisBase;

  constructor(config: ResourceRedisConfig, logger: Logger) {
    super(logger);

    this.log.info('create redis client');

    const options: RedisOptions = {
      lazyConnect: true,
      db: config.db,
    };

    if (config.sentinel.enabled) {
      options.sentinels = config.sentinel
        .hosts
        .split(',')
        .map((host) => ({
          host,
          port: config.sentinel.port,
        }));
      options.name = config.sentinel.name;
      options.sentinelPassword = config.sentinel.password;
      options.password = config.password;
    } else {
      options.port = config.port;
      options.host = config.host;
    }

    this.client = new IORedis(options);
  }

  public get type(): ResourceType {
    return ResourceType.redis;
  }

  public getClient(): RedisBase {
    return this.client;
  }

  public async start(): Promise<void> {
    this.log.info('connecting to redis');
    await this.client.connect();
    this.log.info('connected to redis');
  }

  public async stop(): Promise<void> {
    this.log.info('disconnect from redis');
    this.client.disconnect();
    this.log.info('disconnected from redis');
  }
}
