import { connect, Connection } from 'mongoose';

import { ResourceMongoConfig } from '../../../configs/interfaces';

import { Logger } from '../../libraries/logger';
import { ResourceType } from '../../libraries/constants/resource-types';

import { BaseResource } from '../base';
import { Resource } from '../resource';

export class MongoResource extends BaseResource implements Resource {
  private client?: Connection;

  private config: ResourceMongoConfig;

  constructor(config: ResourceMongoConfig, logger: Logger) {
    super(logger);

    this.log.info('create mongodb connection');
    this.config = config;
  }

  public get type(): ResourceType {
    return ResourceType.mongo;
  }

  public getClient(): Connection {
    if (!this.client) {
      throw new Error('Mongo db client not initialized');
    }

    return this.client;
  }

  public async start(): Promise<void> {
    const connection = await connect(this.makeUri());
    this.client = connection.connection;
    this.log.info('connected to mongodb');
  }

  public async stop(): Promise<void> {
    this.log.info('disconnecting to mongodb');
    await this.client?.close();
    this.log.info('disconnected to mongodb');
  }

  private makeUri(): string {
    const { config } = this;
    const host = new Array<string>();

    if (this.config.auth.enabled) {
      host.push(
        [
          config.auth.user,
          config.auth.password,
        ]
          .join(':'),
      );
    }

    host.push(this.config.nodes);


    return `mongodb+srv://${host.join('@')}/?retryWrites=true&w=majority&appName=${config.db}`;
  }
}
