import { RedisResource } from './redis';
import { MongoResource } from './mongo';

export interface Resources {
  start(): Promise<void>;

  stop(): Promise<void>;

  readonly mongo: MongoResource;

  //todo add redis for cash
  // readonly redis: RedisResource;
}
