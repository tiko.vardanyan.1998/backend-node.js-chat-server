import { Logger as PinoLogger } from 'pino';

import { ResourcesConfig } from '../../configs/interfaces';

import { Logger } from '../libraries/logger';

import { ResourceType } from '../libraries/constants/resource-types';

import { BaseResource } from './base';

import { Resources as ResourcesInterface } from './interfaces';

import { Resource } from './resource';
import { MongoResource } from './mongo';

export class Resources implements ResourcesInterface {
  private readonly log: PinoLogger;

  private readonly logger: Logger;

  private readonly config: ResourcesConfig;

  private readonly resources: Record<ResourceType, BaseResource>;

  constructor(config: ResourcesConfig, logger: Logger) {
    this.config = config;
    this.logger = logger;
    this.log = logger.getLogger('resources');

    this.resources = Object.fromEntries(
      this.init()
        .map<[ResourceType, Resource]>((resource) => ([resource.type, resource])),
    ) as Record<ResourceType, BaseResource>;
  }

  public get mongo(): MongoResource {
    return this.resources[ResourceType.mongo] as MongoResource;
  }

  public async stop(): Promise<void> {
    await Promise.all(
      Object.values(this.resources)
        .map((item) => item.stop()),
    );
  }

  public async start(): Promise<void> {
    await Promise.all(
      Object.values(this.resources)
        .map((item) => item.start()),
    );
  }

  private init(): Array<BaseResource> {
    return [
      new MongoResource(this.config.mongo, this.logger),
    ];
  }
}
