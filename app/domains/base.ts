export interface Base extends Record<string, unknown> {
  id?: string;
}
