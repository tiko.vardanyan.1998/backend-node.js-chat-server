import { Base } from './base';

export interface Message extends Base {
  userId: string
  room: string;
  message: string;
  createdAt: Date;
}

export interface SendMessage {
  room: string;
  message: string;
}
