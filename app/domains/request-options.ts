export interface RequestOptions {
  skip: number;
  limit?: number;
}
