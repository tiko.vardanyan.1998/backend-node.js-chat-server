import { Base } from './base';

export interface User extends Base {
  firstName: string;
  lastName: string;
  username: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface CreateUser {
  firstName: string;
  lastName: string;
  username: string;
  password: string;
}
