import { TransportsConfig } from '../../configs/interfaces';

import { Logger } from '../libraries/logger';


import { Services } from '../services';

import { Transport } from './interfaces/transport';
import { HttpTransport } from './sources/http';
import { JsonWebToken } from '../libraries/jwt/interfaces';
import { WebsocketTransport } from './sources/ws';
import EventEmitter from 'events';


export class Transports {
  private readonly transports: Array<Transport>;

  public constructor(
    services: Services,
    config: TransportsConfig,
    logger: Logger,
    jwt: JsonWebToken,
    eventBus: EventEmitter,
  ) {
    this.transports = [
      new HttpTransport(services, config.http, logger, jwt),
      new WebsocketTransport(config.ws, services, eventBus, logger, jwt),
    ];
  }

  public getTransports(): Array<Transport> {
    return this.transports;
  }

  public async stop(): Promise<void> {
    await Promise.all(
      this.transports
        .map((item) => item.stop()),
    );
  }

  public async start(): Promise<void> {
    await Promise.all(
      this.transports
        .map((item) => item.start()),
    );
  }
}
