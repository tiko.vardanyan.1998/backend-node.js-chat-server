/* eslint-disable no-unused-vars */
import { Response, NextFunction } from 'express';

import { Request } from '../../../../libraries/interfaces/http';

type MiddlewareParameters = (
  req: Request,
  res: Response,
  next: NextFunction,
) => void | Promise<void>

export interface MiddlewareHandler {
    handler: MiddlewareParameters;
}
