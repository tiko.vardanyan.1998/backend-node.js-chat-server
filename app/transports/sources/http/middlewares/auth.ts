/* eslint-disable no-unused-vars */
import { Response, NextFunction } from 'express';
import { Logger as PinoLogger } from 'pino';

import { Request } from '../../../../libraries/interfaces/http';

import { MiddlewareHandler } from './interface';
import { Logger } from '../../../../libraries/logger';
import { JsonWebToken } from '../../../../libraries/jwt/interfaces';

export class AuthMiddleware implements MiddlewareHandler {
  private readonly log: PinoLogger;

  private readonly publicRoutes: Array<string>;

  private readonly jwt: JsonWebToken;

  constructor(logger: Logger, jwt: JsonWebToken) {
    this.log = logger.getLogger('auth-middleware');

    this.jwt = jwt;

    //todo get public routes dynamic
    this.publicRoutes = ['/api/auth/register', '/api/auth/login', '/api/auth/access'];
  }

  public async handler(
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> {
    this.log.info('auth handler fire');

    if (this.publicRoutes.includes(req.path)) {
      return next();
    }

    const token = req.headers['authorization'];

    if (!token) {
      res.status(401).send('Access Token is required!');
      return;
    }

    try {
      const result = await this.jwt.decode(token);

      if (result) {
        req.user = { id: result.id, username: result.username };
      }

      next();
      return;
    } catch (e) {
      this.log.warn(e);
      res.status(401).send('Invalid token!');
      return;
    }
  }
}
