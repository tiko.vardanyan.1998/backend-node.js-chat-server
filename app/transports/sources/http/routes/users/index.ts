import {
  NextFunction, Request as ExpressRequest, Response as ExpressResponse, Router,
} from 'express';

import { Logger as PinoLogger } from 'pino';


import { Logger } from '../../../../../libraries/logger';
import { TransportHttpConfig } from '../../../../../../configs/interfaces';
import { Services } from '../../../../../services';
import { HttpStatusCodes } from '../../../../../libraries/constants/http-status-codes';
import { ErrorMessages } from '../../../../../libraries/constants/error-messages';

export class UserRoute {
  private readonly log: PinoLogger;

  private readonly services: Services;

  private readonly config: TransportHttpConfig;

  constructor(
    services: Services,
    logger: Logger,
    config: TransportHttpConfig,
    ) {
    this.log = logger.getLogger('user-route');

    this.services = services;

    this.config = config;
  }

  public init(): Router {
    this.log.info('Authentication route init');
    const router = Router();

    router
      .post(
        '/register',
        async (
          ...args
        ) => this.create(...args),
      )
      .post(
        '/login',
        async  (
          ...args
        ) => this.login(...args),
      )
      .get(
        '/access',
        async (
          ...args
        ) => this.getAccessToken(...args),
      );

    return router;
  }

  private async create(
    req: ExpressRequest,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      this.log.info('User Create endpoint');

      const user = req.body;

      this.log.info(user);

      const result = await this.services.users.create(user);

      if (result) {
        return res.status(HttpStatusCodes.ok).send({result});
      } else {
        return res.status(HttpStatusCodes.badRequest).send({ message: ErrorMessages.somethingWentWrong });
      }
    } catch (e) {
      this.log.warn(e);
      next(e);
    }
  }

  private async login(
    req: ExpressRequest,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      this.log.info('User Create endpoint');

      const user = req.body;

      this.log.info(user);

      const result = await this.services.users.login(user);

      return res.send(result);
    } catch (e) {
      this.log.warn(e);
      next(e);
    }
  }

  private async getAccessToken(
    req: ExpressRequest,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      this.log.info('Get new access token endpoint');

      const refresh = req.headers['refresh'];

      if (!refresh) {
        res.status(HttpStatusCodes.badRequest).send(ErrorMessages.invalidToken)
      }

      const result = await this.services.users.getAccessTokenFromRefresh(refresh as string);
      return res.send({ access: result });
    } catch (e) {
      this.log.warn(e);
      next(e);
    }
  }
}
