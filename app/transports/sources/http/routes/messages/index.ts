import {
  NextFunction, Request as ExpressRequest, Response as ExpressResponse, Router,
} from 'express';

import { Logger as PinoLogger } from 'pino';

import { Request } from '../../../../../libraries/interfaces/http';


import { Logger } from '../../../../../libraries/logger';
import { TransportHttpConfig } from '../../../../../../configs/interfaces';
import { Services } from '../../../../../services';
import { HttpStatusCodes } from '../../../../../libraries/constants/http-status-codes';
import { ErrorMessages } from '../../../../../libraries/constants/error-messages';

export class MessageRoute {
  private readonly log: PinoLogger;

  private readonly services: Services;

  private readonly config: TransportHttpConfig;

  constructor(
    services: Services,
    logger: Logger,
    config: TransportHttpConfig,
    ) {
    this.log = logger.getLogger('message-route');

    this.services = services;

    this.config = config;
  }

  public init(): Router {
    this.log.info('Message route init');
    const router = Router();

    router
      .get(
        '/',
        async  (
          ...args
        ) => this.getMessages(...args),
      )
      .post(
        '/',
        async (
          ...args
        ) => this.sendMessage(...args),
      )
      .delete(
        '/',
        async (
          ...args
        ) => this.delete(...args),
      );

    return router;
  }

  private async sendMessage(
    req: Request,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      this.log.info('Send Message endpoint');

      const message = req.body;
      const currentUser = req.user;

      if (!currentUser) {
        res.sendStatus(401);
        return;
      }

      this.log.info(message);

      const result = await this.services.messages.send(message, currentUser.id);

      if (result.passed) {
        return res.status(HttpStatusCodes.ok).send('Message has been sent');
      } else {
        return res.status(HttpStatusCodes.badRequest).send(result.message);
      }

    } catch (e) {
      this.log.warn(e);
      next(e);
    }
  }

  private async getMessages(
    req: ExpressRequest,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      this.log.info('Get Messages endpoint');

      const { room, page } = req.query;

      if (!room) {
        res.status(HttpStatusCodes.badRequest).send(ErrorMessages.invalidRoomId);
        return;
      }

      const options = page ? { skip: (+page - 1) * 10 } : { skip: 0 };

      const result = await this.services.messages.getMessages(room as string, options);

      return res.status(HttpStatusCodes.ok).send(result);
    } catch (e) {
      this.log.warn(e);
      next(e);
    }
  }

  private async delete(
    req: Request,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      this.log.info('Delete Messages endpoint');

      const body = req.body;
      const currentUser = req.user;

      if (!currentUser) {
        res.sendStatus(401);
        return;
      }

      if (!body || !body.ids) {
        res.status(HttpStatusCodes.badRequest).send(ErrorMessages.invalidLengthOfMessageIds);
      }

      const result = await this.services.messages.deleteMessages(body.ids as Array<string>, currentUser.id);

      return res.send(`Messages have been successfully deleted, Count of deleted messages is ${result}`);
    } catch (e) {
      this.log.warn(e);
      next(e);
    }
  }
}
