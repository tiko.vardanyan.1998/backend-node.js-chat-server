import {
  Express, NextFunction,
  Request as ExpressRequest,
  Response as ExpressResponse,
} from 'express';
import { Logger as PinoLogger } from 'pino';
import { json } from 'body-parser';
import cors from 'cors';

import { Request } from '../../../../libraries/interfaces/http';

import { Logger } from '../../../../libraries/logger';
import { TransportHttpConfig } from '../../../../../configs/interfaces';
import { UserRoute } from './users';
import { Services } from '../../../../services';
import { AuthMiddleware } from '../middlewares/auth';
import { JsonWebToken } from '../../../../libraries/jwt/interfaces';
import { MessageRoute } from './messages';

export class Routes {
  private readonly app: Express;

  private readonly log: PinoLogger;

  private readonly logger: Logger;

  private readonly services: Services;

  private readonly config: TransportHttpConfig;

  private readonly jwt: JsonWebToken;

  constructor(
    app: Express,
    services: Services,
    config: TransportHttpConfig,
    logger: Logger,
    jwt: JsonWebToken,
  ) {
    this.app = app;

    this.log = logger.getLogger('root-router');
    this.logger = logger;

    this.services = services;

    this.config = config;
    this.jwt = jwt;
  }

  public init(): void {

    const authMiddleware = new AuthMiddleware(this.logger, this.jwt);

    const userRoute = new UserRoute(
      this.services,
      this.logger,
      this.config,
    );
    const messageRoute = new MessageRoute(
      this.services,
      this.logger,
      this.config,
    )

    this.app.use(async (
      req: ExpressRequest,
      res: ExpressResponse,
      next: NextFunction,
    ) => {
      const newReq = req as Request;

      await authMiddleware.handler(newReq, res, next);
    });

    this.app.use(cors());


    this.app.use(json({ limit: '10mb' }));

    this.app.use(Routes.getPath('auth'), userRoute.init());
    this.app.use(Routes.getPath('messages'), messageRoute.init());
  }

  private static getPath(path: string): string {
    return `/api/${path}`;
  }
}
