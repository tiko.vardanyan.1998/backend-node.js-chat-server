import { Server } from 'http';
import express, { Express } from 'express';
import { Logger as PinoLogger } from 'pino';

import { Transport } from '../../interfaces/transport';
import { TransportHttpConfig } from '../../../../configs/interfaces';
import { Logger } from '../../../libraries/logger';
import { Routes } from './routes';
import { Services } from '../../../services';
import { JsonWebToken } from '../../../libraries/jwt/interfaces';



export class HttpTransport implements Transport {
  private readonly log: PinoLogger;

  private readonly logger: Logger;

  private readonly config: TransportHttpConfig;

  private readonly app: Express;

  private readonly services: Services;

  private readonly jwt: JsonWebToken;

  private server?: Server;

  constructor(
    services: Services,
    config: TransportHttpConfig,
    logger: Logger,
    jwt: JsonWebToken,
  ) {
    this.config = config;
    this.logger = logger;
    this.services = services;
    this.jwt = jwt;

    this.log = logger.getLogger('ws-transport');

    this.app = express();
  }

  public async start(): Promise<void> {
    this.log.info('starting http server');
    this.init();

    this.server = this.app.listen(
      this.config.port,
      this.config.host,
      () => this.log
        .info(`server is starting in port: ${this.config.port}`),
    );
  }

  public async stop(): Promise<void> {
    this.server?.close((err) => {
      if (err) {
        this.log.error('', err);
      }

      this.log.info('server is closed');
    });
  }

  private init(): void {
    this.log.info('running instance class route');

    const root = new Routes(
      this.app,
      this.services,
      this.config,
      this.logger,
      this.jwt,
    );
    root.init();
  }
}
