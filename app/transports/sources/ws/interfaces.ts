import { BaseSocketHandler } from './handlers/base';
import { Socket } from './socket';


export enum MessageSource {
  client = 0,
  server = 1,
}

export enum Events {
  connected = 'connected',
  subscribe = 'subscribe',
  listen = 'listen',
  room = 'new_room',
  rooms = 'rooms',
  message = 'message',
  disconnected = 'disconnected',
}

export interface WebSocketMessagePayload {

}

export interface Message {
  event: Events;
  error?: string;
  payload?: WebSocketMessagePayload;
}

export type ImportedSocketHandler = BaseSocketHandler<WebSocketMessagePayload>
  & { new<T extends WebSocketMessagePayload>(socket?: Socket): BaseSocketHandler<T> }

export interface FileImport {
  default: ImportedSocketHandler
}


