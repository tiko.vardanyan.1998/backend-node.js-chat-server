import { WebSocketMessagePayload } from '../../../interfaces';

export interface SendingMessagePayload extends WebSocketMessagePayload {
  // token: string;
  room: string,
  message: string,
}
