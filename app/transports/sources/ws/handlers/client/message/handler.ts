import { ValidationSchema } from 'fastest-validator';
import { BaseSocketHandler } from '../../base';
import { SendingMessagePayload } from './interface';
import { Events, MessageSource } from '../../../interfaces';

export default class SendingMessageHandler extends BaseSocketHandler<SendingMessagePayload> {
  public get event(): Events {
    return Events.message;
  }

  public get type(): MessageSource {
    return MessageSource.client;
  }

  public get schema(): ValidationSchema {
    return {
      room: {
        type: 'string',
      },
      message: {
        type: 'string',
      },
    };
  }

  protected async handler(): Promise<unknown> {
    return this.getSocket().sendMessage(this.getPayload());
  }
}
