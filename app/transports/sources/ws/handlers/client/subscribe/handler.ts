import { ValidationSchema } from 'fastest-validator';
import { BaseSocketHandler } from '../../base';
import { SubscribeMessagePayload } from './interface';
import { Events, MessageSource } from '../../../interfaces';

export default class SubscribeHandler extends BaseSocketHandler<SubscribeMessagePayload> {
  public get event(): Events {
    return Events.subscribe;
  }

  public get type(): MessageSource {
    return MessageSource.client;
  }

  public get schema(): ValidationSchema {
    return {
      rooms: {
        type: 'array',
        items: 'string',
      },
    };
  }

  protected async handler(): Promise<unknown> {
    return this.getSocket().subscribe(this.getPayload());
  }
}
