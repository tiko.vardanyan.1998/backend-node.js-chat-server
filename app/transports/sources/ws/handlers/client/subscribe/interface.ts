import { WebSocketMessagePayload } from '../../../interfaces';

export interface SubscribeMessagePayload extends WebSocketMessagePayload {
  rooms: Array<string>;
}
