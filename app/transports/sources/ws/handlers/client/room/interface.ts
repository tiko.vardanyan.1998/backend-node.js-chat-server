import { WebSocketMessagePayload } from '../../../interfaces';

export interface RoomPayload extends WebSocketMessagePayload {
  room: string;
}
