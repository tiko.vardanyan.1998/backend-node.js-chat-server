import { ValidationSchema } from 'fastest-validator';
import { BaseSocketHandler } from '../../base';
import { RoomPayload } from './interface';
import { Events, MessageSource } from '../../../interfaces';

export default class RoomHandler extends BaseSocketHandler<RoomPayload> {
  public get event(): Events {
    return Events.room;
  }

  public get type(): MessageSource {
    return MessageSource.client;
  }

  public get schema(): ValidationSchema {
    return {
      room: {
        type: 'string',
      },
    };
  }

  protected async handler(): Promise<unknown> {
    return this.getSocket().createNewRoom(this.getPayload());
  }
}
