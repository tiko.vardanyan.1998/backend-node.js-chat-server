import { ValidationSchema } from 'fastest-validator';
import { BaseSocketHandler } from '../../base';
import { Events, MessageSource } from '../../../interfaces';
import { ListenMessagePayload } from './interface';

export default class ListenHandler extends BaseSocketHandler<ListenMessagePayload> {
  get event(): Events {
    return Events.listen;
  }

  get type(): MessageSource {
    return MessageSource.client;
  }

  get schema(): ValidationSchema {
    return {
      token: {
        type: 'string',
      },
    };
  }

  protected async handler(): Promise<unknown> {
    return this.getSocket().startListening(this.getPayload());
  }
}
