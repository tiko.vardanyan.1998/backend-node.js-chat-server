import { WebSocketMessagePayload } from '../../../interfaces';

export interface ListenMessagePayload extends WebSocketMessagePayload {
  token: string;
}
