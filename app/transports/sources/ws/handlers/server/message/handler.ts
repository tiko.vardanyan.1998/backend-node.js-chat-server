import { ValidationSchema } from 'fastest-validator';
import { MessagePayload } from './interfaces';
import { BaseSocketHandler } from '../../base';
import { Events, MessageSource } from '../../../interfaces';

export default class MessageHandler extends BaseSocketHandler<MessagePayload> {
  public get event(): Events {
    return Events.message;
  }

  public get type(): MessageSource {
    return MessageSource.server;
  }

  public get schema(): ValidationSchema {
    return {
      id: {
        type: 'string',
      },
      userId: {
        type: 'string',
      },
      room: {
        type: 'string',
      },
      message: {
        type: 'string',
      },
    };
  }

  protected async handler(): Promise<unknown> {
    return this.socket?.send({
      payload: this.getPayload(),
      event: this.event,
    });
  }
}
