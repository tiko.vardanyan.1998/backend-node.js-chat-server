import { WebSocketMessagePayload } from '../../../interfaces';

export interface MessagePayload extends WebSocketMessagePayload {
  id: string,
  userId: string,
  room: string,
  message: string,
}
