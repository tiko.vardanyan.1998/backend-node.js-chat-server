import Validator, { ValidationSchema } from 'fastest-validator';
import { Logger as PinoLogger } from 'pino';

import { MessageSource, WebSocketMessagePayload } from '../interfaces';
import { Socket } from '../socket';
import { Events } from '../interfaces';


export abstract class BaseSocketHandler<T extends WebSocketMessagePayload> {
  private readonly validator: Validator;

  protected readonly socket?: Socket;

  protected readonly log?: PinoLogger;

  protected payload?: T;

  constructor(socket?: Socket) {
    this.socket = socket;
    this.log = socket?.getLogger();

    this.validator = new Validator();
  }

  protected async beforeHandler(): Promise<unknown> {
    return undefined;
  }

  protected async afterHandler(): Promise<unknown> {
    return undefined;
  }

  public async run(): Promise<void> {
    await this.beforeHandler();
    await this.handler();
    await this.afterHandler();
  }

  public setPayload(data: T): void {
    this.getLogger().debug('Body data', { data });
    const validationResult = this.validator.validate(data, this.schema);

    if (validationResult !== true) {
      this.getLogger()
        .warn(
          `Body data validation result for event: ${this.event}`,
          { result: validationResult },
        );

      throw new Error('Invalid body data');
    }

    this.payload = data;
  }

  protected getPayload(): T {
    if (!this.payload) {
      throw new Error('The body filed not define');
    }

    return this.payload;
  }

  protected getSocket(): Socket {
    if (!this.socket) {
      throw new Error('The socket filed not define');
    }

    return this.socket;
  }

  protected getLogger(): PinoLogger {
    if (!this.log) {
      throw new Error('The logger filed not define');
    }

    return this.log;
  }

  public abstract get event(): Events;

  public abstract get type(): MessageSource;

  public abstract get schema(): ValidationSchema;

  protected abstract handler(): Promise<unknown>;
}
