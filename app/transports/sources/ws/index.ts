import EventEmitter from 'events';
import { App, TemplatedApp, WebSocket } from 'uWebSockets.js';
import { Logger as PinoLogger } from 'pino';


import { Socket } from './socket';
import { Events, Message, MessageSource } from './interfaces';
import { Transport } from '../../interfaces/transport';
import { Services } from '../../../services';
import { TransportWebSocketConfig } from '../../../../configs/interfaces';
import { Logger } from '../../../libraries/logger';
import { JsonWebToken } from '../../../libraries/jwt/interfaces';

export class WebsocketTransport implements Transport {
  private readonly services: Services;

  private readonly config: TransportWebSocketConfig;

  private readonly log: PinoLogger;

  private readonly logger: Logger;

  private readonly server: TemplatedApp;

  private readonly eventBus: EventEmitter;

  private readonly clientMap: Map<WebSocket<{}>, Socket>;

  private readonly jwt: JsonWebToken;

  constructor(
    config: TransportWebSocketConfig,
    services: Services,
    eventBus: EventEmitter,
    logger: Logger,
    jwt: JsonWebToken,
  ) {
    this.config = config;
    this.log = logger.getLogger('ws-server');
    this.logger = logger;
    this.services = services;
    this.eventBus = eventBus;
    this.jwt = jwt;

    this.server = App();
    this.clientMap = new Map<WebSocket<{}>, Socket>();
  }

  public async start(): Promise<void> {
    this.server
      .ws<{}>(
        '/*',
        {
          idleTimeout: this.config.idleTimeout,
          compression: this.config.compression,
          maxPayloadLength: this.config.maxPayloadLength,
          open: async (ws) => this.connect(ws),
          close: async (ws, code, message) => this
            .disconnect(ws, code, message),
          message: async (ws, message, isBinary) => this
            .message(ws, message, isBinary),
        },
      );

    await Socket.registerHandler();

    this.server
      .listen(
        this.config.host,
        this.config.port,
        (listenSocket) => {
          if (listenSocket) {
            this.log
              .info('Listening to port', this.config.port);
          }
        },
      );

    this.eventBus.on(
      Events.disconnected,
      async (params) => {
        await this.disconnect(params.ws, params.code, params.message);
      }
    )
  }

  public async stop(): Promise<void> {
    this.log.info('server is closed');
  }

  private async connect(ws: WebSocket<{}>): Promise<void> {
    const socket = new Socket(ws, this.eventBus, this.services, this.logger, this.jwt);

    this.log.info(`Socket connected id: ${socket.getId()}`);

    this.clientMap.set(ws, socket);
    this.log.info(`New connection. Total connections ${this.clientMap.size}`);
  }

  private async disconnect(ws: WebSocket<{}>, code: number, buffer: ArrayBuffer): Promise<void> {
    const socket = this.clientMap.get(ws);
    const message = Buffer.from(buffer).toString();

    if (socket) {
      await socket.disconnect();
      this.clientMap.delete(ws);
    }

    this.log
      .info(
        `Connection closed code: ${code}, message: ${message}, total connections ${this.clientMap.size}`,
      );
  }

  private async message(ws: WebSocket<{}>, buffer: ArrayBuffer, isBinary: boolean): Promise<void> {
    try {
      const data = isBinary ? this.parseBinaryPayload(buffer) : this.parseTextPayload(buffer);

      if (data === false) {
        return undefined;
      }

      const socket = this.clientMap.get(ws);

      if (socket) {
        const message = data as Message;
        await socket.message(message, MessageSource.client);
      }
    } catch (error) {
      this.log.warn(`Event not parsed, is error: ${error}`);
    }

    return undefined;
  }

  private parseBinaryPayload(message: ArrayBuffer): void {
    this.log.warn('not Implemented');
  }

  // todo add message interfaces
  private parseTextPayload(buffer: ArrayBuffer): unknown | boolean {
    const content = Buffer.from(buffer).toString();
    this.log.debug('Message content: ', content);

    if (content === 'ping') {
      return false;
    }

    return JSON.parse(content);
  }
}
