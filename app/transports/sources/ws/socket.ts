import * as util from 'util';

import { Logger as PinoLogger } from 'pino';

import { WebSocket } from 'uWebSockets.js';
import { v4 as uuidV4 } from 'uuid';
import glob from 'glob';

import EventEmitter from 'events';

import {
  Events,
  FileImport,
  ImportedSocketHandler,
  Message,
  MessageSource,
  WebSocketMessagePayload,
} from './interfaces';
import { BaseSocketHandler } from './handlers/base';
import { Services } from '../../../services';
import { Logger } from '../../../libraries/logger';
import { EventPayload, MessageEventPayload } from '../../../libraries/interfaces/events-payload';
import { MessagePayload } from './handlers/server/message/interfaces';
import { JsonWebToken } from '../../../libraries/jwt/interfaces';
import { SubscribeMessagePayload } from './handlers/client/subscribe/interface';
import { SendingMessagePayload } from './handlers/client/message/interface';
import { RoomPayload } from './handlers/client/room/interface';
import { ListenMessagePayload } from './handlers/client/listen/interface';
import { ClientError } from '../../../libraries/errors/client';
import { ErrorMessages } from '../../../libraries/constants/error-messages';
import { HttpStatusCodes } from '../../../libraries/constants/http-status-codes';

const fileFinder = util.promisify(glob);

export class Socket {
  private readonly connection: WebSocket<{}>;

  private readonly log: PinoLogger;

  private readonly services: Services;

  private readonly eventBus: EventEmitter;

  private readonly id: string;

  private readonly eventListeners: Record<string, EventListener> = {};

  private readonly jwt: JsonWebToken;

  private isStarted = false;

  private rooms: Array<string> = [];

  private userId?: string;

  private static handlers: Partial<
    Record<Events,
      Partial<Record<MessageSource, ImportedSocketHandler>>>
  >;

  constructor(
    connection: WebSocket<{}>,
    eventBus: EventEmitter,
    services: Services,
    logger: Logger,
    jwt: JsonWebToken,
  ) {
    this.log = logger.getLogger('ws-client');
    this.services = services;
    this.connection = connection;
    this.jwt = jwt;

    this.id = uuidV4();
    this.eventBus = eventBus;

    this.init();
  }

  public getLogger(): PinoLogger {
    return this.log;
  }

  public getId(): string {
    return this.id;
  }

  public getUserId(): string {
    if (!this.userId) {
      this.disconnectByError('Web Socket is not authorized!');
      throw new ClientError(ErrorMessages.invalidUserId, HttpStatusCodes.InternalServerError);
    }

    return this.userId;
  }

  public async disconnect(): Promise<void> {
    this.rooms.forEach(
      (room => {
        if (this.eventListeners[room]) {
          this.eventBus.off(
            room,
            this.eventListeners[room],
          );
        }
      })
    )
  }

  public async message(msg: Message, source: MessageSource): Promise<void> {
    const handlers = Socket.handlers[msg.event];

    if (!handlers) {
      this.log
        .warn(`handler for event: ${msg.event} not found`);

      return undefined;
    }

    const HandlerClass = handlers[source];

    if (!HandlerClass) {
      this.log
        .warn(`handler for source: ${source} not found`);

      return undefined;
    }

    const handler: BaseSocketHandler<WebSocketMessagePayload> = new HandlerClass<WebSocketMessagePayload>(this);

    if (handler.type === MessageSource.server) {
      if (!this.rooms.includes((msg.payload as MessagePayload).room)) {
        this.log.debug('Client is not subscribed to this group', msg);

        return undefined;
      }
    }

    if (msg.payload) {
      try {
        handler.setPayload(msg.payload);

        this.log.info(`The ${handler.constructor.name} is fire.`);
      } catch (e) {
        this.log.warn('Something went wrong', e);
      }
    }

    try {
      await handler.run();
    } catch (e) {
      this.log.warn('', e);
    }

    return undefined;
  }

  public send(msg: Message): void {
    this.log.info('WS service send', msg);
    try {
      this.connection.send(JSON.stringify(msg));
    } catch (e) {
      this.log.warn('', e);
    }
  }

  public async startListening(params: ListenMessagePayload): Promise<void> {

    try {
      const jwtResult = await this.jwt.decode(params.token);

      this.userId = jwtResult.id;
    } catch (e) {
      this.disconnectByError('Token is required!');
    }

    this.log.info('WS service listen');
    if (this.isStarted) {
      return;
    }

    this.isStarted = true;

    this.send({
      event: Events.rooms,
      payload: {
        rooms: this.services.messages.getRooms(),
      }
    })
  }

  public async subscribe(params: SubscribeMessagePayload): Promise<void> {
    this.log.info('WS service subscribe', { rooms: params.rooms });
    this.rooms = this.rooms.concat(params.rooms);

    this.rooms.forEach((room) => {
      const listener = this.messageHandler;

      this.eventListeners[room] = async (
        params: EventPayload,
      ) => listener.call(this, params);

      this.eventBus.on(
        room,
        this.eventListeners[room],
      );

      return undefined;
    });
  }

  public async sendMessage(params: SendingMessagePayload): Promise<void> {
      this.eventBus.emit(
        Events.message,
        { msg: params, userId: this.getUserId() },
      );

      await this.services.messages.send({room: params.room, message: params.message}, this.getUserId());
  }

  public createNewRoom(params: RoomPayload): void {
    this.eventBus.emit(
      Events.room,
      params,
    );
  }

  private init(): void {
    try {
      this.send({
        event: Events.connected,
        payload: {
          id: this.id,
        },
      });
    } catch (e) {
      this.log.warn('', e);
    }
  }

  private async messageHandler(params: MessageEventPayload): Promise<void> {
    if (this.getUserId() === params.userId) {
      return;
    }

    const payload: MessagePayload = {
      id: params.id,
      userId: params.userId,
      room: params.room,
      message: params.message,
    };

    const msg: Message = {
      event: Events.message,
      payload,
    };

    await this.message(msg, MessageSource.server);

    return undefined;
  }

  public static async registerHandler(): Promise<void> {
    if (Socket.handlers) {
      return undefined;
    }

    Socket.handlers = {};

    const files = await fileFinder('**/handler.{js,ts}', { cwd: __dirname, absolute: true });

    await Promise.all(
      files.map(async (handlerPath) => {
        const file: FileImport = await import(handlerPath);
        const HandlerClass = file.default;
        const instance: BaseSocketHandler<WebSocketMessagePayload> = new HandlerClass<WebSocketMessagePayload>();

        if (!Socket.handlers[instance.event]) {
          Socket.handlers[instance.event] = {};
        }

        const handlers = Socket.handlers[instance.event];

        if (handlers) {
          handlers[instance.type] = HandlerClass;
        }
      }),
    );

    return undefined;
  }

  private disconnectByError(msg: string): void {
    this.log.warn(msg);

    this.eventBus.emit(
      Events.disconnected,
      {
        ws: this.connection,
        code: 401,
        message: [],
      }
    )
    this.connection.end(401, msg);
  }
}
