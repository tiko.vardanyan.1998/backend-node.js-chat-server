import jwt, { Algorithm } from 'jsonwebtoken';
import { v4 as uuIdv4 } from 'uuid';

import { AppJwtConfig } from '../../../configs/interfaces';

import { JsonWebToken, Payload, Result, TokenType } from './interfaces';
import { ClientError } from '../errors/client';
import { HttpStatusCodes } from '../constants/http-status-codes';
import { ErrorMessages } from '../constants/error-messages';

export class Jwt implements JsonWebToken {
  private readonly config: AppJwtConfig;

  constructor(config: AppJwtConfig) {
    this.config = config;
  }

  public async encode(user: { id: string, username: string }): Promise<Result> {
    const payload: Payload = {
      id: user.id,
      username: user.username,
      type: TokenType.access,
    };

    const access = await this.generateToken(payload);

    payload.type = TokenType.refresh;
    const refresh = await this.generateToken(payload);

    return {
      access,
      refresh,
    };
  }

  public async decode(token: string): Promise<Payload> {
    return new Promise<Payload>((resolve, reject) => {
      jwt.verify(
        token,
        this.config.secret,
        {
          algorithms: [
            this.config.algorithm as Algorithm,
          ],
        },
        (error, payload) => {
          if (error) {
            reject(error);
          }

          resolve(payload as Payload);
        },
      );
    });
  }

  public async generateAccessFromRefresh(refresh: string): Promise<string> {
    try {
      const result = await this.decode(refresh);

      const payload: Payload = {
        id: result.id,
        username: result.username,
        type: TokenType.access,
      };

      return this.generateToken(payload);
    } catch (e) {
      throw new ClientError(ErrorMessages.invalidToken, HttpStatusCodes.badRequest);
    }
  }

  private async generateToken(payload: Payload): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      jwt.sign(
        payload,
        this.config.secret,
        {
          algorithm: this.config.algorithm as Algorithm,
          expiresIn: payload.type === TokenType.access ? this.config.expire.access : this.config.expire.refresh,
          jwtid: uuIdv4(),
        },
        (error, token) => {
          if (error) {
            reject(error);
          }

          if (token) {
            resolve(token);
          }
        },
      );
    })
  }
}
