export enum TokenType {
  none = 0,
  access = 1,
  refresh = 2,
}

export interface Payload {
  id: string;
  username: string;
  type: TokenType;
}

export interface Result {
  access: string;
  refresh: string
}

export interface JsonWebToken {
  encode(user: { id: string, username: string }): Promise<Result>;
  generateAccessFromRefresh(refresh: string): Promise<string>;
  decode(token: string): Promise<Payload>
}
