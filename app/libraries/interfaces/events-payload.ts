export interface EventPayload {}

export type EventListener = (params: EventPayload) => Promise<void>;



export interface MessageEventPayload extends EventPayload {
  id: string;
  userId: string;
  room: string;
  message: string;
}
