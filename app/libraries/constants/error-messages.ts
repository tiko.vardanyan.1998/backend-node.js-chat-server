export enum ErrorMessages {
  alreadyExistsUsername = 'Username already exists.',
  invalidToken = 'Invalid Token',
  invalidUserId = 'Invalid user Id',
  invalidRoomId = 'Invalid room id',
  invalidLengthOfMessageIds = 'Invalid length of Message ids',
  wrongPassword = 'Wrong password',
  somethingWentWrong = 'Something went wrong',
  notFound = 'Not found',
  userNotFound = 'User not found',
}
