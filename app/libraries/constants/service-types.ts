export enum ServiceTypes {
  none = 0,
  users = 1,
  messages = 2,
}
