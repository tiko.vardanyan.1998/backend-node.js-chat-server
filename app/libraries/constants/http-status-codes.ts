/* eslint-disable no-unused-vars */
export enum HttpStatusCodes {
  none = 0,
  InternalServerError = 500,
  badRequest = 400,
  ok = 200,
}
