export enum ResourceType {
  none = 0,
  redis = 1,
  mongo = 2,
}
