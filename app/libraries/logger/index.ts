import path from 'path';
import stream from 'stream';

import pino, { Logger as PinoLogger, LoggerOptions } from 'pino';


import {
  Level, multistream, Streams,
} from 'pino-multi-stream';

import { LoggerConfig } from '../../../configs/interfaces';

const StackTraceOffset = 2;
const { symbols: { asJsonSym } } = pino;
type PinoArg = Array<string | number | { package: string; caller?: string }>;

export class Logger {
  private readonly config: LoggerConfig;

  private readonly loggerStream?: stream.Duplex;

  constructor(config: LoggerConfig) {
    this.config = config;
  }

  getLogger(module?: string): PinoLogger {
    let level = this.config.levels.any;
    const moduleName = module || 'main';

    if (this.config.levels[moduleName]) {
      level = this.config.levels[moduleName];
    }

    const options: LoggerOptions = {
      mixin: (): { package: string } => ({
        package: moduleName,
      }),
      base: null,
      timestamp: (): string => `,"time":"${new Date()}"`,
      level,
      messageKey: 'message',
      formatters: {
        level: (label): { level: string } => ({ level: label }),
      },
    };

    const streams: Streams = [];

      streams.push({
        stream: pino.destination(1),
        level: level as Level,
      });


    if (this.loggerStream) {
      streams.push({
        level: 'warn',
        stream: this.loggerStream,
      });
    }

    const instance = pino(options, multistream(streams));

    if (['debug', 'trace'].includes(level)) {
      return new Proxy<pino.Logger>(
        instance,
        {
          get: (target, name): () => string => {
            if (name === asJsonSym) {
              return (...args: Array<any>): string => {
                const info = (
                  args[0] || Object.create(null)
                ) as { package: string; caller?: string };
                const error = new Error();
                info.caller = error.stack && error.stack
                  .split('\n')
                  .filter((s) => !s.includes(path.join('node_modules', 'pino')))[StackTraceOffset]
                  .replace(path.resolve(__dirname, '..'), '')
                  .replace(/^(.+\()(.*)(:\d+\))$/i, '$2')
                  .replace(/^\s+at\s+/i, '');
                // @ts-ignore
                return target[asJsonSym]
                  .call<pino.Logger, PinoArg, string>(target, ...args);
              };
            }
            // @ts-ignore
            return target[name];
          },
        },
      );
    }

    return instance;
  }
}
