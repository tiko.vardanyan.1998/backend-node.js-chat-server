
import { Message as MessageDomain } from '../../../domains/message';
import { SendMessage } from '../../../domains/message';
import { RequestOptions } from '../../../domains/request-options';

export interface Messages {
  send(message: SendMessage, userId: string): Promise<{ passed: boolean, message?: string }>;
  getMessages(room: string, options: RequestOptions): Promise<Array<MessageDomain>>;
  deleteMessages(ids: Array<string>, userId: string): Promise<number>;
  getRooms(): Array<string>;
  createRoom(room: string): void;
}
