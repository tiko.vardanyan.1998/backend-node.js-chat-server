import EventEmitter from 'events';

import { ServiceTypes } from '../../../libraries/constants/service-types';
import { BaseService } from '../../base';

import { Messages } from './interfaces';
import { Message as MessageDomain, SendMessage } from '../../../domains/message';
import { RequestOptions } from '../../../domains/request-options';
import { Repositories } from '../../../repositories/interfaces/repositories';
import { Resources } from '../../../resources/interfaces';
import { Logger } from '../../../libraries/logger';
import { AppConfig } from '../../../../configs/interfaces';
import { MessageEventPayload } from '../../../libraries/interfaces/events-payload';


export class MessagesService extends BaseService implements Messages {
  private readonly eventBus: EventEmitter;

  private readonly rooms: Array<string>;

  constructor(
    repositories: Repositories,
    resources: Resources,
    logger: Logger,
    config: AppConfig,
    eventBus: EventEmitter
  ) {
    super(repositories, resources, logger, config);

    this.eventBus = eventBus;

    //todo keep rooms in db
    this.rooms = ['default-room'];
  }

  public get type(): ServiceTypes {
    return ServiceTypes.messages;
  }

  public getRooms(): Array<string> {
    return this.rooms;
  }

  public async send(message: SendMessage, userId: string): Promise<{ passed: boolean, message?: string }> {
    if (!this.rooms.includes(message.room)) {
      return {
        passed: false,
        message: 'Room does not exist!',
      }
    }

    const result = await this.repositories.messages.create(message, userId);

    const payload: MessageEventPayload = {
      id: result._id.toString(),
      userId,
      ...message,
    }

    this.eventBus.emit(message.room, payload);

    return { passed: true };
  }

  public async getMessages(room: string, options: RequestOptions): Promise<Array<MessageDomain>> {
    return this.repositories.messages.getMessages(room, { ...options, limit: 10 });
  }

  public async deleteMessages(ids:Array<string>, userId:string): Promise<number> {
    return this.repositories.messages.deleteMessages(ids, userId);
  }

  public createRoom(room: string): void {
    if (!this.rooms.includes(room)) {
      this.rooms.push(room);
    }
  }
}
