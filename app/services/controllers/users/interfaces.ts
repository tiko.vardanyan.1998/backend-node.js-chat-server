import { CreateUser } from '../../../domains/user';
import { Result as TokenPair } from '../../../libraries/jwt/interfaces';

export interface Users {
  create(user: CreateUser): Promise<{ userId: string }>;
  login(user: {username: string, password: string}): Promise<TokenPair>;
  getAccessTokenFromRefresh(refresh: string): Promise<string>;
}
