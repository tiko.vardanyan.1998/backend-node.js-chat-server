import * as bcrypt from 'bcrypt';
import { ServiceTypes } from '../../../libraries/constants/service-types';

import { BaseService } from '../../base';

import { Users } from './interfaces';
import { CreateUser } from '../../../domains/user';
import { JsonWebToken, Result as TokenPair } from '../../../libraries/jwt/interfaces';
import { AppConfig } from '../../../../configs/interfaces';
import { Logger } from '../../../libraries/logger';
import { Repositories } from '../../../repositories/interfaces/repositories';
import { Resources } from '../../../resources/interfaces';
import { ClientError } from '../../../libraries/errors/client';
import { HttpStatusCodes } from '../../../libraries/constants/http-status-codes';
import { ErrorMessages } from '../../../libraries/constants/error-messages';

export class UsersService extends BaseService implements Users {
    protected readonly jwt: JsonWebToken;

  constructor(
    repositories: Repositories,
    resources: Resources,
    logger: Logger,
    config: AppConfig,
    jwt: JsonWebToken,
  ) {
    super(
      repositories,
      resources,
      logger,
      config,
    );
    this.jwt = jwt;
  }

  public get type(): ServiceTypes {
    return ServiceTypes.users;
  }

  public async create(user: CreateUser): Promise<{ userId: string }> {
      return this.repositories.users.create({ ...user, password: this.generateHash(user.password) });
  }

  public async login(user: {username: string, password: string}): Promise<TokenPair> {
    const userDocument = await this.repositories.users.findByUsername(user.username);

    const validationResult = await this.validateHash(user.password, userDocument.password);

    if (!validationResult) {
      throw new ClientError(ErrorMessages.wrongPassword, HttpStatusCodes.badRequest);
    }

    return this.jwt.encode({ id: userDocument._id.toString(), username: userDocument.username });
  }

  public async getAccessTokenFromRefresh(refresh: string): Promise<string> {
    return this.jwt.generateAccessFromRefresh(refresh);
  }

  private generateHash(password: string): string {
    return bcrypt.hashSync(password, 10);
  }
  private async validateHash(password: string, hash: string): Promise<boolean> {
    if (!password || !hash) {
      return Promise.resolve(false);
    }

    return bcrypt.compare(password, hash);
  }
}
