import { Users } from '../controllers/users/interfaces';

export interface Services {
  readonly users: Users;
  // readonly messages: Messages;

  start(): Promise<void>;
  stop(): Promise<void>;
}
