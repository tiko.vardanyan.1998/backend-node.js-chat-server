import { Logger as PinoLogger } from 'pino';
import _ from 'lodash';

import { AppConfig } from '../../configs/interfaces';

import { ServiceTypes } from '../libraries/constants/service-types';

import { Logger } from '../libraries/logger';

import { Repositories } from '../repositories/interfaces/repositories';

import { Resources } from '../resources/interfaces';

import { Service } from './interfaces/service';

export class BaseService implements Service {
  protected readonly log: PinoLogger;

  protected readonly repositories: Repositories;

  protected readonly resources: Resources;

  protected readonly config: AppConfig;

  constructor(
    repositories: Repositories,
    resources: Resources,
    logger: Logger,
    config: AppConfig,
  ) {
    this.log = logger.getLogger(_.kebabCase(this.constructor.name));
    this.resources = resources;
    this.repositories = repositories;
    this.config = config;
  }

  public get type(): ServiceTypes {
    throw new Error('Service type getter not implemented');
  }

  public async start(): Promise<void> {
  }

  public async stop(): Promise<void> {
  }
}
