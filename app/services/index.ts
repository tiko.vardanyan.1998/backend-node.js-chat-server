/* eslint-disable @typescript-eslint/no-unused-vars */
import { Logger as PinoLogger } from 'pino';

import { AppConfig } from '../../configs/interfaces';

import { Logger } from '../libraries/logger';

import { ServiceTypes } from '../libraries/constants/service-types';

import { Repositories } from '../repositories/interfaces/repositories';
import { Resources } from '../resources/interfaces';

import { BaseService } from './base';

import { Services as ServicesInterface } from './interfaces/services';
import { UsersService } from './controllers/users';
import { Users } from './controllers/users/interfaces';
import { JsonWebToken } from '../libraries/jwt/interfaces';
import { MessagesService } from './controllers/messages';
import { Messages } from './controllers/messages/interfaces';
import EventEmitter from 'events';

export class Services implements ServicesInterface {
  private readonly log: PinoLogger;

  private readonly logger: Logger;

  private readonly config: AppConfig;

  private readonly services: Record<ServiceTypes, BaseService>;

  constructor(
    repositories: Repositories,
    resources: Resources,
    logger: Logger,
    config: AppConfig,
    jwt: JsonWebToken,
    eventBus: EventEmitter,
  ) {
    this.logger = logger;
    this.log = logger.getLogger('services');
    this.config = config;

    this.services = Object.fromEntries(
      this.init(
        repositories,
        resources,
        logger,
        config,
        jwt,
        eventBus,
      )
        .map(
          (service) => [service.type, service],
        ),
    ) as Record<ServiceTypes, BaseService>;
  }

  public get users(): Users {
    return this.services[ServiceTypes.users] as UsersService;
  }

  public get messages(): Messages {
    return this.services[ServiceTypes.messages] as MessagesService;
  }

  public async start(): Promise<void> {
    await Promise.all(
      Object.values(this.services)
        .map((service) => service.start()),
    );
  }

  public async stop(): Promise<void> {
    await Promise.all(
      Object.values(this.services)
        .map((service) => service.stop()),
    );
  }

  private init(
    repositories: Repositories,
    resources: Resources,
    logger: Logger,
    config: AppConfig,
    jwt: JsonWebToken,
    eventBus: EventEmitter
  ): Array<BaseService> {
    return [
      new UsersService(
        repositories,
        resources,
        logger,
        config,
        jwt,
      ),
      new MessagesService(
        repositories,
        resources,
        logger,
        config,
        eventBus,
      ),
    ];
  }
}
