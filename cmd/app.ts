/* eslint-disable no-console */
import path from 'path';

import { AppServer } from '../app/app';

async function main() {
  const rootPath = path.join(__dirname, '..');

  const application = new AppServer(rootPath);

  process.once('SIGTERM', async () => {
    console.log('SIGTERM received');

    await application.stop();

    process.exit(0);
  });
  process.once('SIGINT', async () => {
    console.log('SIGINT received');

    await application.stop();

    process.exit(0);
  });

  await application.init();

  return application.start();
}

main()
  .catch(console.error);
